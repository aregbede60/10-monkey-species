# 10 Monkey Species

The project is a fine grain classification of different species of a Monkey. The dataset consists of two files, training and validation. Each folder contains 10 subforders labeled as n0~n9, each corresponding a species form Wikipedia's monkey cladogr